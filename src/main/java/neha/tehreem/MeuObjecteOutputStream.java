package neha.tehreem;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class MeuObjecteOutputStream extends ObjectOutputStream {

    // Constructor
    public MeuObjecteOutputStream(OutputStream out) throws IOException {
        super(out);
    }

    // Constructor sense parametres
    protected MeuObjecteOutputStream() throws IOException, SecurityException {
        super();

    }

    // Redefinicio del mètode d'escirure la capçalera per a que no faci res.
    @Override
    protected void writeStreamHeader() throws IOException {
        // AQUEST MÈTODE ORIGINAL ESCRIU UNA CAPÇALERA
        // AQUI EL REESCRIC EN BLANC. NO FA RES.
        // EL MÈTODE EXSITEIX .PERÒ NO ESCRIU LA CAPÇALERA
        // I NES PERMETRÀ AFEGIR UN OBJECTE A UN FITXER DE FORMA NORMAL.


    }
}
