package neha.tehreem;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.UserPrincipal;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Thread.sleep;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class Fitxers {

    public void escriuFitxerText(
            String fitxer,
            String text,
            boolean afegir
    ) {
        try (
                FileWriter out = new FileWriter(fitxer, afegir)) // true → afegim al fitxer
        {
            out.write(text + "\n"); // escrivim al fitxer
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public String retornaFitxerText(String fitxer) {

        String linia; // per recollir la línia
        String text = "";

        try {
            // BLOC DE TRY .. CATCH
            // NECESSARI PER UTILITZAR STREAMS
            FileReader in = new FileReader(fitxer);
            {
                Scanner input = new Scanner(in);
                while (input.hasNextLine()) { // Mentre hi hagen línies a l'arxiu ...
                    linia = input.nextLine(); // Agafa una línia
                    text = text + linia + "\n"; // guardem les dades de l’usuari
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return text;
    }


    public List<String> retornaFitxerTextLlista(String fitxer) {

        String linia; // per recollir la línia
        List<String> text = new ArrayList<>();        // contingut del fitxer
        try {
            // BLOC DE TRY .. CATCH
            // NECESSARI PER UTILITZAR STREAMS
            FileReader in = new FileReader(fitxer);
            {
                Scanner input = new Scanner(in);
                while (input.hasNextLine()) { // Mentre hi hagen línies a l'arxiu ...
                    linia = input.nextLine(); // Agafa una línia
                    text.add(linia); // guardem les dades de l’usuari
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return text;
    }


    //<editor-fold desc="JAVA.IO">
    //java.io
    //https://docs.oracle.com/javase/7/docs/api/java/io/File.html
    //https://docs.oracle.com/javase/7/docs/api/java/io/FileReader.html
    //https://docs.oracle.com/javase/7/docs/api/java/io/FileWriter.html

    /**
     * Escriu un fitxer de text
     *
     * @param fitxer ruta del fitxer
     * @param text   text a escriure
     * @param afegir true: afegeix al fitxer. False: Substitueix tot el fitxer.
     */
    public void escriuFitxerTextIO(
            String fitxer,
            String text,
            boolean afegir
    ) {
        try (
                FileWriter out = new FileWriter(fitxer, afegir)) // true → afegim al fitxer
        {
            out.write(text + "\n"); // escrivim al fitxer
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Escriu un fitxer de text
     *
     * @param fitxer ruta del fitxer
     * @param text   llista d'String a escriure
     * @param afegir true: afegeix al fitxer. False: Substitueix tot el fitxer.
     */
    public void escriuFitxerTextLlistaIO(
            String fitxer,
            List<String> text,
            boolean afegir) {

        for (String linia : text) {
            escriuFitxerTextIO(fitxer, linia, afegir);
        }
    }


    /**
     * Retorna el contingut d'un fitxer de text
     *
     * @param fitxer ruta del fitxer
     * @return String amb el contingut del fitxer
     */
    public String retornaFitxerTextIO(String fitxer) {

        String linia; // per recollir la línia
        String text = "";

        try {
            // BLOC DE TRY .. CATCH
            // NECESSARI PER UTILITZAR STREAMS
            FileReader in = new FileReader(fitxer);
            {
                Scanner input = new Scanner(in);
                while (input.hasNextLine()) { // Mentre hi hagen línies a l'arxiu ...
                    linia = input.nextLine(); // Agafa una línia
                    text = text + linia + "\n"; // guardem les dades de l’usuari
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return text;
    }

    /**
     * Retorna el contingut d'un fitxer de text en una llista d'Strings
     *
     * @param fitxer ruta del fitxer
     * @return String amb el contingut del fitxer
     */
    public List<String> retornaFitxerTextLlistaIO(String fitxer) {

        String linia; // per recollir la línia
        List<String> text = new ArrayList<>();        // contingut del fitxer
        try {
            // BLOC DE TRY .. CATCH
            // NECESSARI PER UTILITZAR STREAMS
            FileReader in = new FileReader(fitxer);
            {
                Scanner input = new Scanner(in);
                while (input.hasNextLine()) { // Mentre hi hagen línies a l'arxiu ...
                    linia = input.nextLine(); // Agafa una línia
                    text.add(linia); // guardem les dades de l’usuari
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return text;
    }

    //</editor-fold>


    //<editor-fold desc="ADMINISTRACIÓ DE FITXERS IO">


    /**
     * Comprova si existeix un fitxer o directori
     *
     * @param fitxer fitxer o directori a comprovar
     * @return true si existeix. False si no existeix
     */
    public String obteRutaIO(String fitxer) {
        File file = new File(fitxer);
        return file.getAbsolutePath();
    }

    @Deprecated
    /**
     * Comprova si existeix un fitxer o directori
     *
     * @param directori  o fitxer a comprovar
     * @return true si existeix. False si no existeix
     */
    public boolean esDirectoriIO(String directori) {
        File file = new File(directori);
        return file.isDirectory();
    }

    @Deprecated
    /**
     * renombra un fitxer per un altre (o el mou a un altre lloc)
     * @param fitxer
     * @param nouNom
     */
    public void renombraFitxerIO(String fitxer, String nouNom) {
        java.io.File file = new java.io.File(fitxer);
        java.io.File file2 = new java.io.File(nouNom);
        if (file.renameTo(file2)) {
            System.out.println("Fitxer renombrat: " + file.getName() + " a " + file2.getName());
        } else {
            System.out.println("No s'ha pogut renombrar el fitxer.");
        }
    }


    /**
     * Llista els fitxers d'un directori
     *
     * @param directori directori a llistar
     */
    public void llistaFitxersIO(String directori) {
        File file = new File(directori);
        String[] fitxers = file.list();
        for (String fitxer : fitxers) {
            System.out.println(fitxer);
        }
    }


    /**
     * Comprova si existeix una ruta a un arxiu/directori amb java io
     *
     * @param ruta ruta a comprovar
     * @return true si existeix. False si no existeix
     * @deprecated perque existeix millor mètode
     */
    @Deprecated
    public boolean existeixIO(String ruta) {
        File arxiu = new File(ruta);
        return arxiu.exists();
    }

    /**
     * Crea un directori amb java io
     *
     * @param directori directori a crear
     * @deprecated perquè existeix millor mètode
     */
    @Deprecated
    public void creaDirectoriIO(String directori) {
        File arxiu = new File(directori);
        arxiu.mkdir();
    }

    /**
     * eliminia un fitxer/directori amb java.io
     *
     * @param ruta ruta del fitxer/directori a eliminar
     * @deprecated perque existeix millor mètode
     */
    @Deprecated
    public void eliminaFitxerDirectoriIO(String ruta) {
        File arxiu = new File(ruta);
        arxiu.delete();
    }

    /**
     * Movem (renombrem) un fitxer a un altre nom (lloc)
     *
     * @param fitxerOriginal nom del fitxer a renombrar
     * @param fitxerDesti    nom nou del fitxer que renombrarem
     * @deprecated perque existeix millor mètode
     */
    @Deprecated
    public void moureFitxerDirectoriIO(String fitxerOriginal,
                                       String fitxerDesti) {

        File fOriginal = new File(fitxerOriginal);
        File fDesti = new File(fitxerDesti);
        fOriginal.renameTo(fDesti);
    }

    /**
     * Retorna les propietats bàsiques d'un Fitxer (
     * si és directori, o un arxiu, si és readable, si és writable, si es pot executar i si està ocult
     * El CSV estarà muntat d'aquesta manera:
     * isReadable+";"+isWritable+";"+isExecutable+";"+isDirectory+";"+isFile+";"+isHidden;
     * per exemple: true;true;false;false;true;false
     *
     * @param fitx fitxer a analitzar
     * @return String amb les propietats comentades, amb format csv ;
     * @throws IOException Excepció
     */
    public String propietatsFitxer(String fitx) {

        //isReadable+";"+isWritable+";"+isExecutable+";"+isDirectory+";"+isFile+isHidden
        // per exemple:
        // true;true;false;false;true;false

        File fitxer = new File(fitx);
        String text;

        // és directori
        boolean isDirectory = fitxer.isDirectory();

        // és arxiu
        boolean isFile = fitxer.isFile();

        // està ocult
        boolean isHidden = fitxer.isHidden();

        // podem llegir el fitxer?
        boolean isReadable = fitxer.canRead();

        // podem executar-lo?
        boolean isExecutable = fitxer.canExecute();

        // podem modificar-lo?
        boolean isWritable = fitxer.canWrite();

        text = isReadable + ";" + isWritable + ";" +
                isExecutable + ";" + isDirectory + ";" +
                isFile + ";" + isHidden;

        return text;
    }


    //</editor-fold>


    // <editor-fold defaultstate="collapsed" desc="JAVA NIO">
    // ***********  //
    //   FUNCIONS   //
    // ***********  //


    /**
     * Retorna un Path a partir d'una ruta
     *
     * @param fitxer ruta del fitxer
     * @return Path amb la ruta
     */
    public Path obtePath(String fitxer) {
        return Paths.get(fitxer);
    }


    /**
     * per llistar els arxius d'un directori i la resta baix d'ell de forma recursiva
     *
     * @param fitx ruta del directori
     */
    public String retornaArbreDirectoris(String fitx) {
        //
        // NO NIVELL ENCARA
        // NO UTILITZAT EN PROGRAMA EXEMPLE
        //
        File arx = new File(fitx);
        String directoris = "";
        File listFile[] = arx.listFiles();
        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    directoris = directoris + listFile[i].toString() + "\n";        //  RECURSIU  //
                } else {
                    directoris = directoris + listFile[i].getPath() + "\n";
                }
            }
        }
        return directoris;
    }


    /**
     * Escriu el text "text" al fitxer "fitx"
     * !!EN AQUEST CAS S'ESCRIU EN UN JOC DE CARÀCTERS: UTF-8 !!
     *
     * @param fitx   ruta del fitxer de text
     * @param text   cadena String a escriure al fitxer
     * @param afegir True: afegeix al fitxer. False: Substitueix tot el fitxer.
     * @throws IOException excepció si hi ha algun error
     */
    public void escriuTextFitxer(String fitx,
                                 String text,
                                 boolean afegir)
            throws IOException {

        escriuTextFitxer(fitx, text, "UTF-8", afegir);
    }

    /**
     * Escriu el text "text" al fitxer "fitx"
     * !!EN AQUEST CAS S'ESCRIU EN UN JOC DE CARÀCTERS: UTF-8 !!
     *
     * @param fitx   ruta del fitxer de text
     * @param text   cadena String a escriure al fitxer
     * @param afegir True: afegeix al fitxer. False: Substitueix tot el fitxer.
     * @throws IOException excepció si hi ha algun error
     */
    public void escriuTextFitxer(String fitx,
                                 String text,
                                 String charset,
                                 boolean afegir) throws IOException {

        Path path = Paths.get(fitx);

        List<StringBuffer> dades = new ArrayList();
        dades.add(new StringBuffer(text));

        // Escrivim dintre del fitxer finalment
        if (afegir)
            Files.write(path, dades, Charset.forName(charset), Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
        else
            Files.write(path, dades, Charset.forName(charset));
    }


    /**
     * Retorna el fitxer en un text utilitzant NIO
     *
     * @param fitx ruta del fitxer
     * @return String amb el contingut del fitxer
     * @throws IOException          Excepció d'E/S
     * @throws InterruptedException Excepció
     */
    public String retornaContingutFitxer(String fitx,
                                         String charset) throws IOException, InterruptedException {

        String text = "";
        Path path = Paths.get(fitx);
        String linia;

        // Llegeix d'un fitxer
        if (existeix(fitx)) {
            // LLEGIM EL CONTINGUT DEL FITXER
            Charset jocCaracter = Charset.forName(charset);
            BufferedReader reader = Files.newBufferedReader(path, jocCaracter);
            while ((linia = reader.readLine()) != null) {
                text = text + linia + "\n";
            }
        } else {
            sleep(5);
            System.err.println("El fitxer " + fitx + " NO existeix");
        }
        return text;
    }


    /**
     * Retorna el fitxer en un text utilitzant NIO
     *
     * @param fitx ruta del fitxer
     * @return String amb el contingut del fitxer
     * @throws IOException          Excepció d'E/S
     * @throws InterruptedException Excepció
     */
    public List<String> retornaContingutFitxerLlista(String fitx) throws IOException, InterruptedException {
        return retornaContingutFitxerLlista(fitx, "UTF-8");
    }


    /**
     * Retorna el fitxer en un text utilitzant NIO
     *
     * @param fitx ruta del fitxer
     * @return String amb el contingut del fitxer
     * @throws IOException          Excepció d'E/S
     * @throws InterruptedException Excepció
     */
    public List<String> retornaContingutFitxerLlista(String fitx,
                                                     String charset) throws IOException, InterruptedException {

        Path path = Paths.get(fitx);
        String linia;
        List<String> llista = new ArrayList<>();

        // Llegeix d'un fitxer
        if (existeix(fitx)) {
            // LLEGIM EL CONTINGUT DEL FITXER
            Charset jocCaracter = Charset.forName(charset);
            BufferedReader reader = Files.newBufferedReader(path, jocCaracter);
            while ((linia = reader.readLine()) != null) {
                llista.add(linia);
            }
        } else {
            sleep(5);
            System.err.println("El fitxer " + fitx + " NO existeix");
        }
        return llista;
    }


    /**
     * Retorna el fitxer en un text utilitzant NIO
     *
     * @param fitx ruta del fitxer
     * @return String amb el contingut del fitxer
     * @throws IOException          Excepció d'E/S
     * @throws InterruptedException Excepció
     */
    public String retornaContingutFitxer(String fitx) throws IOException, InterruptedException {
        return retornaContingutFitxer(fitx, "UTF-8");
    }


    /**
     * Mostra per consola el contingut del fitxer de text
     *
     * @param fitx Ruta del fitxer
     * @throws IOException          Excepció d'Entrada/Sortida
     * @throws InterruptedException Excepció d'Interrupció
     */
    public void mostraContingutFitxer(String fitx) throws IOException, InterruptedException {
        System.out.println(retornaContingutFitxer(fitx));
    }

    // </editor-fold>


    //<editor-fold desc="ADMINISTRACIÓ DE FITXERS NIO">

    /**
     * retorna true o false si el directori o fitxer passat per paràmetre existeix
     *
     * @param dir directori o fitxer a comprovar
     * @return true si existeix. false si no existeix
     */
    @Deprecated
    public boolean ComprovaRuta(String dir) {
        // Comprova la existència d'un directori o un fitxer
        return existeix(dir);    // Files --> import java.nio.file.Files
    }

    /**
     * retorna true o false si el directori o fitxer passat per paràmetre existeix
     *
     * @param ruta directori o fitxer a comprovar
     * @return true si existeix. false si no existeix
     */
    public boolean existeix(String ruta) {
        // Comprova la existència d'un directori o un fitxer
        Path fitxer = Paths.get(ruta);         // Path --> import java.nio.file.Path;
        // Paths --> import java.nio.file.Paths
        return Files.exists(fitxer);    // Files --> import java.nio.file.Files
    }

    /**
     * Crea un directori a la ruta indicada
     *
     * @param ruta ruta del directori
     * @throws IOException Excepció d'Entrada / Sortida
     */
    public void creaDirectori(String ruta) throws IOException {
        Path path = Paths.get(ruta);         // Path --> import java.nio.file.Path;
        // Paths --> import java.nio.file.Paths
        Files.createDirectory(path);    // Files --> import java.nio.file.Files
    }

    /**
     * Mostra per pantalla algunes propietats de fitxers
     *
     * @param fitx ruta del fitxer
     * @throws IOException Excepció d'Entrada Sortida
     */
    public String propietariFitxer(String fitx) throws IOException {

        Path path = Paths.get(fitx);

        // propietari
        UserPrincipal propietari = Files.getOwner(path);

        return propietari.toString();

    }

    /**
     * Elimina un fitxer o directori passat per paràmetre
     *
     * @param fitx fitxer o directori a eliminar
     * @throws IOException excepció d'entrada/sortida
     */
    public void eliminarFitxerDirectori(String fitx) throws IOException {

        // Borrem un fitxer o directori
        // try{
        Path path = Paths.get(fitx);
        Files.delete(path);                 // No veiem encara la clàusula try .. catch .. finally

        // }catch(NoSuchFileException x){
        // // No existeix el fitxer
        // }catch(DirectoryNotEmptyException x){
        //     // El directori no està buit i no es pot borrar
        // }catch (IOException x){
        //     // Altres tipus d'errors. Per exemple no tenir permissos per borrar
        //}
    }

    /**
     * Copia un fitxer o directori a un altre lloc
     *
     * @param origen fitxer o directori a copiar
     * @param desti  nou fitxe o directori que es copiarà
     * @throws IOException excepció d'Entrada/Sortida
     */
    public void copiarFitxerDirectori(String origen,
                                      String desti) throws IOException {

        Path pathOrigen = Paths.get(origen);
        Path pathDesti = Paths.get(desti);

        Files.copy(pathOrigen, pathDesti, REPLACE_EXISTING);

    }

    /**
     * Mou un fitxer o directori a altre lloc
     *
     * @param origen fitxer a moure
     * @param desti  fitxer on es mourà
     * @throws IOException excepció d'E/S
     */
    public void moureFitxerDirectori(String origen,
                                     String desti) throws IOException {

        // copia un fitxer (origen) en un fitxer (desti)
        Path pathOrigen = Paths.get(origen);
        Path pathDesti = Paths.get(desti);

//        if (existeix(desti)){
//            eleccio=Pregunta("El arxiu ja existeix.\n" +
//                    "vols sobreescriure'l?");
//            if (eleccio==0)
        Files.move(pathOrigen, pathDesti, REPLACE_EXISTING);
//            else
//                MissatgeInfo("OK. No sobreescrivim el fitxer");

//        }
    }

    /**
     * Retorna en String en format csv separat en punts i coma amb les metadades de:
     * Tamany, Directory, esFitxer,esSimbòlic,estàOcult,ÚltimaModificació,Propietari
     *
     * @param fitx ruta del fitxer a analitzar
     * @return String amb les metadades
     * @throws IOException excepció d'E/S
     */
    public String metadadesFitxer(String fitx) throws IOException {

        String cadena;
        if (existeix(fitx)) {
            Path path = Paths.get(fitx);
            cadena = String.valueOf(Files.size(path));
            cadena = cadena + ";" + Files.isDirectory(path);
            cadena = cadena + ";" + Files.isRegularFile(path);
            cadena = cadena + ";" + Files.isSymbolicLink(path);
            cadena = cadena + ";" + Files.isHidden(path);
            cadena = cadena + ";" + Files.getLastModifiedTime(path);
            cadena = cadena + ";" + Files.getOwner(path);
        } else {
            cadena = "No existeix el fitxer";
        }
        return cadena;
    }
    //</editor-fold>


    // ESCRIURE OJBECTES FITXERS
    public void esciruObjecteFitxer(Object obj, String arxiu, boolean afegir) throws IOException {

        if (!afegir || !existeix(arxiu)) {
            // Per crear el primer objecte
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(arxiu, afegir));
            out.writeObject(obj);
        } else {
            // per afegir la resta d'objectes. Per afegir també posem true despres el mon del fitxer
            MeuObjecteOutputStream out2 = new MeuObjecteOutputStream(new FileOutputStream(arxiu, afegir));
            out2.writeObject(obj);
        }

    }

    public List<Object> retornaFitxerObjecteEnLlista(String fitxer) {

        List<Object> lObjs = new ArrayList<>();

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(fitxer));

            do {
                Object obj = in.readObject();
                lObjs.add(obj);
            } while (in != null);
            in.close();
            in = null;

        } catch (Exception e) {
            e.toString();
        }
        return lObjs;
    }


}
