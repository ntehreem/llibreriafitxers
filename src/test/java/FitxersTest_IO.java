import neha.tehreem.Fitxers;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FitxersTest_IO {

    static final Fitxers f = new Fitxers();
    static String nomFitxer = "fitxerTest.txt";

    @AfterAll
    static void tearDown() {
        f.eliminaFitxerDirectoriIO(nomFitxer);
    }

    @Test
    @Order(1)
    void escriuFitxerText_test() {
        f.escriuFitxerTextIO(nomFitxer, "Hola", false);
        assertEquals("Hola\n", f.retornaFitxerTextIO(nomFitxer));
        f.escriuFitxerTextIO(nomFitxer, "Adeu", true);
        assertEquals("Hola\nAdeu\n", f.retornaFitxerTextIO(nomFitxer));
    }

    @Test
    @Order(2)
    void retornaFitxerText_test() {
        assertEquals("Hola\nAdeu\n", f.retornaFitxerTextIO(nomFitxer));
    }

    @Test
    @Order(3)
    void retornaFitxerTextLlista_test() {
        assertEquals(2, f.retornaFitxerTextLlistaIO(nomFitxer).size());
        assertEquals("Hola", f.retornaFitxerTextLlistaIO(nomFitxer).get(0));
        assertEquals("Adeu", f.retornaFitxerTextLlistaIO(nomFitxer).get(1));
    }

    @Test
    @Order(4)
    void escriuFitxerTextLlista_test() {
        List<String> llista = new ArrayList<>();
        llista.add("Hola");
        llista.add("Adeu");
        f.eliminaFitxerDirectoriIO(nomFitxer);
        f.escriuFitxerTextLlistaIO(nomFitxer, llista, true);
        //f.escriuFitxerTextLlista(nomFitxer, f.retornaFitxerTextLlista(nomFitxer),);
        assertEquals("Hola\nAdeu\n", f.retornaFitxerTextIO(nomFitxer));

    }

    @Test
    @Order(5)
    void existeix_test() {
        assertTrue(f.existeixIO(nomFitxer));
    }

    @Test
    @Order(1000)
    void eliminaFitxer_test() {
        f.eliminaFitxerDirectoriIO(nomFitxer);
        assertFalse(f.existeixIO(nomFitxer));
    }

    @Test
    @Order(6)
    void obteRuta_test() {
        try {
            assertTrue(f.obteRutaIO(nomFitxer).contains(nomFitxer));
        } catch (Exception e) {
            fail("No s'ha pogut obtenir la ruta del fitxer");
        }
    }

    @Test
    @Order(7)
    void creaDirectori_test() {
        f.creaDirectoriIO("directoriTest");
        assertTrue(f.existeixIO("directoriTest"));
        assertTrue(f.esDirectoriIO("directoriTest"));
        f.eliminaFitxerDirectoriIO("directoriTest");
        assertFalse(f.existeixIO("directoriTest"));
    }

    @Test
    @Order(8)
    void renombraFitxer_test() {
        f.escriuFitxerTextIO(nomFitxer, "Hola", false);
        f.renombraFitxerIO(nomFitxer, "fitxerTest2.txt");
        assertFalse(f.existeixIO(nomFitxer));
        assertTrue(f.existeixIO("fitxerTest2.txt"));
        f.eliminaFitxerDirectoriIO("fitxerTest2.txt");
    }

    @Test
    @Order(9)
    void mouFitxer_test() {
        f.escriuFitxerTextIO(nomFitxer, "Hola", false);
        f.renombraFitxerIO(nomFitxer, "fitxerTest2.txt");
        assertFalse(f.existeixIO(nomFitxer));
        assertTrue(f.existeixIO("fitxerTest2.txt"));
        f.eliminaFitxerDirectoriIO("fitxerTest2.txt");
    }

    @Test
    @Order(10)
    void llistaFitxers_test() {
        f.llistaFitxersIO(".");
    }

    @Test
    @Order(11)
    void obtePath_test() {
        assertNotNull(f.obtePath(nomFitxer));
    }


}

